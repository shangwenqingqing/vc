package com.ibs.vc.demo.controller;

import com.ibs.vc.demo.entity.App;
import com.ibs.vc.demo.dto.CreateAppRequest;
import com.ibs.vc.demo.service.AppService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author WQ Shang
 */
@Api
@RestController
@RequestMapping("/v1/apps")
public class AppController {
    private final AppService appService;

    public AppController(AppService appService) {
        this.appService = appService;
    }

    @PostMapping
    public App create(@RequestBody @Valid CreateAppRequest request) {
        return appService.create(request.getName());
    }

    @GetMapping
    public List<App> list() {
        return appService.list();
    }

    @DeleteMapping(value = "/{appId}")
    public void delete(@PathVariable String appId) {
        appService.delete(appId);
    }

    @PutMapping(value = "{appId}")
    public App update(@PathVariable String appId, @RequestBody CreateAppRequest request) {
        return appService.update(appId, request);
    }

}
