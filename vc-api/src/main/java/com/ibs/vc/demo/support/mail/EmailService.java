package com.ibs.vc.demo.support.mail;


/**
 * @author WQ Shang
 */
public interface EmailService {

    /**
     * 发送简单邮件
     */
    void sendSimpleMail(Email email);

}

