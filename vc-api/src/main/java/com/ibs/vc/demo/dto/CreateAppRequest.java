package com.ibs.vc.demo.dto;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author YQ.Huang
 */
public class CreateAppRequest {
    @NotBlank
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
