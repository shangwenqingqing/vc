package com.ibs.vc.demo.service;

import com.ibs.vc.demo.config.VcProperties;
import com.ibs.vc.demo.dto.CheckCodeResponse;
import com.ibs.vc.demo.dto.CreateCodeRequest;
import com.ibs.vc.demo.entity.App;
import com.ibs.vc.demo.entity.VerificationCode;
import com.ibs.vc.demo.repository.CodeRepository;
import com.ibs.vc.demo.support.CodeGenerator;
import com.ibs.vc.demo.support.CodeSender;
import com.ibs.webapi.exception.BadRequestException;
import org.springframework.stereotype.Service;

/**
 * @author WQ Shang
 */
@Service
public class CodeService {

    private final CodeRepository codeRepository;
    private final AppService appService;
    private final CodeGenerator codeGenerator;
    private final CodeSender codeSender;
    private final VcProperties props;

    public CodeService(CodeRepository codeRepository, AppService appService, CodeGenerator codeGenerator, CodeSender codeSender, VcProperties props) {
        this.codeRepository = codeRepository;
        this.appService = appService;
        this.codeGenerator = codeGenerator;
        this.codeSender = codeSender;
        this.props = props;
    }

    public void create(String appId, CreateCodeRequest request) {
        App app = appService.find(appId);
        if (app == null) {
            throw new BadRequestException("App " + appId + " not found");
        }
        VerificationCode code = codeRepository.findByAppIdAndPurposeAndMail(
                appId, request.getCodePurpose(), request.getMail());
        if (code == null) {
            code = app.createCode(request.getMail(), request.getCodePurpose());
            String newCode = codeGenerator.generate(props.getLength());
            code.setCode(newCode);
            code.setExpiration(props.getExpiration());
            code.setName(request.getUserName());
        }
        codeRepository.save(code);
        codeSender.send(code,app.getName());
    }

    public CheckCodeResponse check(String appId, String mail, String purpose, String code) {
        VerificationCode result = codeRepository.findByAppIdAndPurposeAndMail(appId, purpose, mail);
        if (result == null) {
            return new CheckCodeResponse(false, "Code not exists");
        }
        if (result.check(code)) {
            return new CheckCodeResponse(true, "Correct code");
        } else {
            return new CheckCodeResponse(false, "Incorrect code");
        }
    }
}
