package com.ibs.vc.demo.config;

import com.ibs.vc.demo.entity.CodeStyle;
import org.hibernate.validator.constraints.Range;
import org.springframework.boot.context.properties.ConfigurationProperties;

import javax.validation.constraints.NotNull;

/**
 * @author WQ Shang
 */
@ConfigurationProperties(prefix = "vc")
public class VcProperties {

    /**
     * 验证码风格
     */
    @NotNull
    private CodeStyle codeStyle = CodeStyle.NUMBER;

    /**
     * 验证码存留时间/s,默认：600
     */
    @Range(min = 1, max = 2 * 60 * 60)
    private long expiration = 500;

    /**
     * 验证码长度,默认：6
     */
    @Range(min = 4, max = 8)
    private int length = 6;

    public void setCodeStyle(CodeStyle codeStyle) {
        this.codeStyle = codeStyle;
    }

    public long getExpiration() {
        return expiration;
    }

    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }

    public CodeStyle getCodeStyle() {
        return codeStyle;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
}
