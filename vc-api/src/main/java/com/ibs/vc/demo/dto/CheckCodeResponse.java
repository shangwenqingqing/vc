package com.ibs.vc.demo.dto;

/**
 * @author YQ.Huang
 */
public class CheckCodeResponse {
    private boolean pass;
    private String error;

    public CheckCodeResponse(boolean pass, String error) {
        this.pass = pass;
        this.error = error;
    }

    public boolean isPass() {
        return pass;
    }

    public void setPass(boolean pass) {
        this.pass = pass;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
