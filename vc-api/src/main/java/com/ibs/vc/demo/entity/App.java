package com.ibs.vc.demo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;

/**
 * @author WQ Shang
 */
@RedisHash
public class App implements Serializable {
    @Id
    private String id;
    @Indexed
    private String name;

    App() {
    }

    public App(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VerificationCode createCode(String mail, String codePurpose) {
        return new VerificationCode(mail, codePurpose, id);
    }
}
