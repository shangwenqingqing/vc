package com.ibs.vc.demo.dto;

import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author  WQ Shang
 */
public class CreateCodeRequest {
    @ApiModelProperty("验证邮箱")
    @Email
    private String mail;
    @ApiModelProperty("验证码用途")
    private String codePurpose;
    @ApiModelProperty("收件人")
    @NotBlank
    private String userName;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getCodePurpose() {
        return codePurpose;
    }

    public void setCodePurpose(String codePurpose) {
        this.codePurpose = codePurpose;
    }
}
