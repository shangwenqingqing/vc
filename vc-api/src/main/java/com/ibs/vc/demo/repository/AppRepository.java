package com.ibs.vc.demo.repository;

import com.ibs.vc.demo.entity.App;
import org.springframework.data.keyvalue.repository.KeyValueRepository;
import org.springframework.stereotype.Repository;

/**
 * @author WQ Shang
 */
@Repository
public interface AppRepository extends KeyValueRepository<App, String> {
    App findByName(String name);
}