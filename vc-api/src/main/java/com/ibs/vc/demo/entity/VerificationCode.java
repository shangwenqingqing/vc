package com.ibs.vc.demo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.TimeToLive;
import org.springframework.data.redis.core.index.Indexed;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author WQ Shang
 */
public class VerificationCode implements Serializable {
    @Id
    private String id;
    @Indexed
    private String mail;
    private String name;
    @Indexed
    private String purpose;
    @Indexed
    private String appId;
    private String code;

    @TimeToLive
    private long expiration;

    public VerificationCode(String mail, String purpose, String appId) {
        this.mail = mail;
        this.purpose = purpose;
        this.appId = appId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appName) {
        this.appId = appName;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public long getExpiration() {
        return expiration;
    }

    public void setExpiration(long expiration) {
        this.expiration = expiration;
    }

    public boolean check(String code) {
        return Objects.equals(this.code, code);
    }

    @Override
    public String toString() {
        return "VerificationCode{" +
                "id='" + id + '\'' +
                ", mail='" + mail + '\'' +
                ", purpose=" + purpose +
                ", appId='" + appId + '\'' +
                ", code='" + code + '\'' +
                ", expiration=" + expiration +
                '}';
    }
}
