package com.ibs.vc.demo.controller;

import com.ibs.vc.demo.dto.CheckCodeResponse;
import com.ibs.vc.demo.dto.CreateCodeRequest;
import com.ibs.vc.demo.service.CodeService;
import io.swagger.annotations.Api;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


/**
 * @author WQ Shang
 */
@Api
@RestController
@RequestMapping("/v1/apps/{appId}/codes")
public class CodeController {
    private final CodeService codeService;

    public CodeController(CodeService codeService) {
        this.codeService = codeService;
    }

    @PostMapping
    public void create(@PathVariable String appId, @RequestBody @Valid CreateCodeRequest request) {
        codeService.create(appId, request);
    }

    @GetMapping("/check")
    public CheckCodeResponse check(@PathVariable String appId, @RequestParam @Valid String mail, @RequestParam String purpose, @RequestParam String code) {
        return codeService.check(appId, mail, purpose, code);
    }

}
