package com.ibs.vc.demo.entity;

public enum CodeStyle {
    NUMBER,
    LETTER,
    LETTER_NUMBER
}
