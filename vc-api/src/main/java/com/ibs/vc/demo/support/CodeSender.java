package com.ibs.vc.demo.support;

import com.ibs.vc.demo.entity.VerificationCode;
import com.ibs.vc.demo.support.mail.Email;
import com.ibs.vc.demo.support.mail.EmailService;
import org.springframework.boot.autoconfigure.mail.MailProperties;

/**
 * @author YQ.Huang
 */
public class CodeSender {

    private final EmailService emailService;
    private final MailProperties properties;

    public CodeSender(EmailService emailService, MailProperties properties) {
        this.emailService = emailService;
        this.properties = properties;
    }

    public void send(VerificationCode code, String appName) {
        String contain = getContain(code, appName);
        String subject = getSubject(code.getPurpose(), appName);
        Email email = new Email(properties.getUsername());
        email.setSubject(subject);
        email.setText(contain);
        email.setTo(code.getMail());
        emailService.sendSimpleMail(email);
    }

    private static String getSubject(String codePurpose, String appName) {
        return appName + codePurpose + "验证码";
    }

    private static String getContain(VerificationCode code, String appName) {
        return "亲爱的" +
                code.getName() +
                "您好：\n以下为【" +
                appName +
                "】为您提供的" +
                code.getPurpose() +
                "验证码，有效时间为" +
                code.getCode() +
                "有效时间为" +
                code.getExpiration() +
                "秒。\n感谢使用";
    }
}
