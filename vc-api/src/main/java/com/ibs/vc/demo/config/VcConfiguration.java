package com.ibs.vc.demo.config;

/**
 * @author WQ Shang
 */

import com.ibs.vc.demo.entity.CodeStyle;
import com.ibs.vc.demo.support.CodeGenerator;
import com.ibs.vc.demo.support.CodeSender;
import com.ibs.vc.demo.support.mail.EmailService;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author WQ Shang
 */
@Configuration
@EnableConfigurationProperties(VcProperties.class)
public class VcConfiguration {

    private final VcProperties props;
    private final EmailService emailService;
    private final MailProperties properties;

    public VcConfiguration(VcProperties props, EmailService emailService, MailProperties properties) {
        this.props = props;
        this.emailService = emailService;
        this.properties = properties;
    }

    @Bean
    public CodeGenerator codeGenerator() {
        String base = null;
        CodeStyle codeStyle = props.getCodeStyle();
        switch (codeStyle) {
            case LETTER_NUMBER:
                base = "abcdefghijklmnopqrstuvwxyz0123456789";
                break;
            case NUMBER:
                base = "0123456789";
                break;
            case LETTER:
                base = "abcdefghijklmnopqrstuvwxyz";
                break;
            default:
                break;
        }
        return new CodeGenerator(base);
    }

    @Bean
    CodeSender codeSender() {
        return new CodeSender(emailService, properties);
    }
}
