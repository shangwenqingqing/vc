package com.ibs.vc.demo.support;

import java.util.Random;

/**
 * @author WQ Shang
 */

public class CodeGenerator {

    private String base;
    private Random random = new Random();

    public CodeGenerator(String base) {
        this.base = base;
    }

    public String generate(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(base.length());
            sb.append(base.charAt(number));
        }
        return sb.toString();
    }
}
