package com.ibs.vc.demo.repository;

import com.ibs.vc.demo.entity.VerificationCode;
import org.springframework.data.keyvalue.repository.KeyValueRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface CodeRepository extends KeyValueRepository<VerificationCode, String> {
    VerificationCode findByAppIdAndPurposeAndMail(String appId, String purpose, String mail);
}
