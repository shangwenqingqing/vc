package com.ibs.vc.demo.service;


import com.ibs.vc.demo.dto.CreateAppRequest;
import com.ibs.vc.demo.entity.App;
import com.ibs.vc.demo.repository.AppRepository;
import com.ibs.webapi.exception.BadRequestException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author WQ Shang
 */
@Service
public class AppService {
    private final AppRepository appRepository;

    public AppService(AppRepository appRepository) {
        this.appRepository = appRepository;
    }

    public App create(String name) {
        if (appRepository.findByName(name) != null) {
            throw new BadRequestException("app name is not invalid");
        } else {
            App app = new App(name);
            return appRepository.save(app);
        }
    }

    public App find(String appId) {
        return appRepository.findOne(appId);
    }

    public List<App> list() {
        return (List<App>) appRepository.findAll();
    }

    public void delete(String appId) {
        appRepository.delete(appId);
    }

    public App update(String appId, CreateAppRequest request) {
        App app = find(appId);
        if (app == null) {
            throw new BadRequestException("App " + appId + " is not found");
        }else if (appRepository.findByName(request.getName()) != null) {
            throw new BadRequestException("app name is not invalid");
        }
        app.setName(request.getName());
        return appRepository.save(app);
    }

}
